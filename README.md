<div align="center">
    <img src="docs/logo.svg" alt="Capital One Canada Microsite Platform" width="200px" height="200px"/>
</div>

# Gatsby Capital One Starter

A high performance starter for Capital One microsites using [Gatsby](https://github.com/gatsbyjs/gatsby/).


## GatsbyJS V2

This starter is based on GatsbyJS V2, which brings progressive web app features such as automatic code and data splitting (by route), prefetching, with service worker/offline-first support and PRPL pattern.

More information in the [announcement](https://www.gatsbyjs.org/blog/2018-09-17-gatsby-v2/).

## Features

- Blazing fast loading times thanks to pre-rendered HTML and automatic chunk loading of JS files
- Integrated Capital One style guide
  - Integrated Icons font
  - Integrated Optimist font
  - Integrated Onestart-Foundation
- SASS/SCSS styling
- [NetlifyCMS](https://www.netlifycms.org/docs/intro/) support for creating/editing posts via admin panel
- Separate components for everything
- High configurable
- Posts in Markdown
  - Code syntax highlighting
  - Embedded YouTube videos
  - Embedded Tweets
- Responsive design with mobile first approach
- Progressive Web App (WPA) for slow to fast networks
  - Web App Manifest support
  - Automatic image transformation and size optimization
- Tag Management with Google Tag Manager
- Snowplow analytics support [TODO: similar to gatsby-plugin-google-analytics]
- SEO
  - Sitemap generation
  - robots.txt
  - General description tags
  - Schema.org JSONLD (Google Rich Snippets)
  - OpenGraph Tags (Facebook/Google+/Pinterest)
  - Twitter Tags (Twitter Cards)
  - RSS feeds
- Offline support
- Development tools
  - ESLint for linting
  - Prettier for code style
  - Remark-Lint for linting Markdown
  - write-good for linting English prose
  - gh-pages for deploying to GitHub pages
  - CodeClimate configuration file and badge
- Netlify deploy configuration

## Getting Started

Install this starter (assuming [Gatsby](https://github.com/gatsbyjs/gatsby/) is installed and updated) by running from your CLI:

```sh
gatsby new YourProjectName https://gitlab.com/capitalone-canada/gatsby-c1-starter.git
npm install # or yarn install
npm run develop # or gatsby develop
```


Alternatively:

```sh
git clone https://gitlab.com/capitalone-canada/gatsby-c1-starter.git YourProjectName # Clone the project
cd YourProjectname
rm -rf .git # So you can have your own changes stored in VCS.
npm install # or yarn install
npm run develop # or gatsby develop
```

## Configuration

Edit the export object in `data/site-config`:

WARNING: Make sure to edit `static/robots.txt` to include your domain for the sitemap!

## NetlifyCMS

First of all, make sure to edit `static/admin/config.yml` and add your [GitHub/GitLab/NetlifyId credentials](https://www.netlifycms.org/docs/authentication-backends/):

```yml
backend:
  name: gitlab # Refer to https://www.netlifycms.org/docs/authentication-backends/ for auth backend list and instructions
  branch: master # Branch to update
  repo: capitalone-canada/gatsby-c1-starter # Repo for pushing new commits. Make sure to replace with your repo!
```

You can visit `/admin/` after and will be greeted by a login dialog (depending on the auth provider you ave chosen above).

For NetlifyCMS specific issues visit the [official documentation](https://www.netlifycms.org/docs/intro/).

## Theming


