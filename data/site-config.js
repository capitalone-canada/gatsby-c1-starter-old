module.exports = {
  siteTitle: "Capital One Canada - Microsite", // Site title.
  siteTitleShort: "C1 Microsite", // Short site title for homescreen (PWA). Preferably should be under 12 characters to prevent truncation.
  siteTitleAlt: "Capital One Canada - Microsite", // Alternative site title for SEO.
  siteLogo: "/logos/logo-1024.png", // Logo used for SEO and manifest.
  siteUrl: "https://microsite.capitalone.partners", // Domain of your website without pathPrefix.
  pathPrefix: "/gatsby-c1-starter", // Prefixes all links. For cases when deployed to example.github.io/gatsby-c1-starter/.
  fixedFooter: false, // Whether the footer component is fixed, i.e. always visible
  siteDescription: "A high performance starter for Capital One microsites using GatsbyJS", // Website description used for RSS feeds/meta description tag.
  siteRss: "/rss.xml", // Path to the RSS file.
  siteTrackingID: "", // Tracking code ID for analytics.
  postDefaultCategoryID: "All", // Default category for posts.
  dateFromFormat: "YYYY-MM-DD", // Date format used in the frontmatter.
  dateFormat: "DD/MM/YYYY", // Date format for display.
  copyright: "Copyright © 2018. Capital One Canada" // Copyright string for the footer of the website and RSS feed.
};
