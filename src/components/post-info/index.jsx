import React, { Component } from "react";
import { Link } from "gatsby";
import _ from "lodash";

class PostInfo extends Component {
  render() {
    const { postNode } = this.props;
    const post = postNode.frontmatter;
    return (
      <div className="post-info">
        <header>
          <p>Published on {postNode.fields.date}</p>
          <p>{postNode.timeToRead} min read</p>
        </header>
        <Link
          className="category-link"
          to={`/categories/${_.kebabCase(post.category)}`}
        >
          <span>In category {post.category}</span>
        </Link>
      </div>
    );
  }
}

export default PostInfo;
