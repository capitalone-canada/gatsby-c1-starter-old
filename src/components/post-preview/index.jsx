import React, { Component } from "react";
import { Link } from "gatsby";

class PostPreview extends Component {
  render() {
    const { postInfo } = this.props;
    return (
      <div key={postInfo.path} className="md-grid md-cell md-cell--12">
        <Link style={{ textDecoration: "none" }} to={postInfo.path}>postInfo.title</Link>
        <header>
          <p>Published on {postInfo.date}</p>
          <p>{postInfo.timeToRead} min read</p>
        </header>

        <div>
          {postInfo.excerpt}
          <p className="tags">{postInfo.tags}</p>
        </div>
      </div>
    );
  }
}

export default PostPreview;
