import React, { Component } from "react";
import { Link } from "gatsby";
import config from "../../../data/site-config";

class SiteFooter extends Component {
  render() {
    const url = config.siteRss;
    const { copyright } = config;
    return (
      <footer id="site-footer">
        <div className="copyright">
          <h4>{copyright}</h4>
        </div>
        <div className="rss">
          <Link to={url}>Subscribe</Link>
        </div>
      </footer>
    );
  }
}

export default SiteFooter;
