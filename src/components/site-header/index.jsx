import React, { Component } from "react";
import SiteNavigation from '../site-navigation';

class SiteHeader extends Component {
  render() {
    return (
      <div>
        <img src="" alt="mSIP - Microsite Platform" />
        <SiteNavigation />
      </div>
    )
  }
}

export default SiteHeader;
