import React, { Component } from "react";
import { Link } from "gatsby";

class SiteNavigation extends Component {
  render() {
    return (
      <nav>
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/about">About</Link></li>
        </ul>
      </nav>
    )
  }
}

export default SiteNavigation;
