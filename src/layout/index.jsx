import React from "react";
import Helmet from "react-helmet";
import config from "../../data/site-config";

import SiteHeader from '../components/site-header';
import SiteFooter from '../components/site-footer';

import "./index.scss";
import "./global.scss";

export default class MainLayout extends React.Component {
  render() {
    const { children } = this.props;
    return (
      <div id="outer-wrapper">
        <Helmet>
          <meta name="description" content={config.siteDescription} />
        </Helmet>

        <SiteHeader />
        <div className="content-wrapper">
          {children}
        </div>
        <SiteFooter />
      </div>
    );
  }
}
