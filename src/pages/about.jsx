import React, { Component } from "react";
import Helmet from "react-helmet";
import Layout from "../layout";
import config from "../../data/site-config";

class AboutPage extends Component {
  render() {
    return (
      <Layout location={this.props.location} title="About">
        <Helmet>
          <title>{`About | ${config.siteTitle}`}</title>
          <link rel="canonical" href={`${config.siteUrl}/about/`} />
        </Helmet>

        <div className="about-container">
          <div className="about-wrapper">
            <img
              src={config.userAvatar}
              className="about-img"
              alt={config.userName}
            />
            <p className="about-text md-body-1">{config.userDescription}</p>
          </div>
        </div>
      </Layout>
    );
  }
}

export default AboutPage;
